;;; org2html -- compile org to html output
;;; Commentary:
;;; position les export et autorise le bind pour le footer dans le org
;;; Code:
(org-mode)
(require 'ox-html)
; exclusion des tags qui ne devrait pas être dans la version html
(setq org-export-exclude-tags '("latex" "noexport"))
(org-html-export-to-html)
;;; org2html.el ends here
