// Not needed anymore in this version, but ...
// var update_link = function () {
//     $('a').each(function() {
//       if ($(this).prop('href').startsWith("javascript:")) {
//         return; /* Don't touch JavaScript URLs */
//       }
//       if ($(this).prop('href') == window.location.href) {
//         $(this).addClass('current');
//       } else if ($(this).hasClass('current')) {
//         $(this).addClass('visited_after_load');
//         $(this).removeClass('current');
//       }
//     });
// };

function isElementInViewport (el) {
    // special bonus for those using jQuery
    if (typeof jQuery === "function" && el instanceof jQuery) {
        el = el[0];
    }

    var rect = el.getBoundingClientRect();

    return (
        rect.top >= 0 &&
        rect.left >= 0 &&
        rect.bottom <= (window.innerHeight || document.documentElement.clientHeight) && /*or $(window).height() */
        rect.right <= (window.innerWidth || document.documentElement.clientWidth) /*or $(window).width() */
    );
}

$(function(){
    $('#table-of-contents').toc({
        'highlightOffset': 0,
	// Overload default to add the pushState hack below
	'smoothScrolling': function(target, options, callback) {
	    var oldHist = history.pushState;
	    var shouldRestore = false;
	    // internally, smoothScroller uses history.pushState, if
	    // present, to update the URL. Unfortunately, this does
	    // not move the carret in carret browsing, and harms
	    // accessibility. Let smoothScrolling think that
	    // history.pushState is not available.
	    history.pushState = null;
	    $(target).smoothScroller({
		offset: options.scrollToOffset
	    }).on('smoothScrollerComplete', function() {
		callback();
		// The callback is called twice, only restore the
		// second time.
		if (shouldRestore) {
		    history.pushState = oldHist;
		}
		shouldRestore = true;
	    });
	},
        'onHighlight': function(el) {
	    // Keep track of which section was visited.
            el.addClass('visited_after_load');
	    // Scroll to make the element visible if needed.
            if (!isElementInViewport(el)) {
                $('#table-of-contents').animate({
                        scrollTop: el.offset().top
                    }, 1000);
            }
        }
    });
    //window.onhashchange = update_link;
    //update_link();
});
