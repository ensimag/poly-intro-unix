(org-mode)
; minted requière l'installation de pygmentize (+joli, -portable)
;(setq org-latex-listings 'minted)
(require 'ox-latex)
;(add-to-list 'org-latex-packages-alist '("newfloat" "minted"))
;; exclusion des tags qui ne devrait pas être dans la version latex 
(setq org-export-exclude-tags '("html" "noexport"))
(add-to-list 'org-latex-classes
	     '("ensimag-book" "\\documentclass{book}"
	       ("\\chapter{%s}" . "\\chapter*{%s}")
	       ("\\section{%s}" . "\\section*{%s}")
	       ("\\subsection{%s}" . "\\subsection*{%s}")
	       ("\\subsubsection{%s}" . "\\subsubsection*{%s}")
	       ("\\paragraph{%s}" . "\\paragraph*{%s}")))
(org-latex-export-to-latex)
