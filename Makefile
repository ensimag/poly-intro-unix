CHAMILO_USER=moym

RUBBER=${shell which rubber}
RFLAGS=-Wrefs -Wmisc -c shell_escape --unsafe

SVG=${wildcard *.svg}
JS=${wildcard *.js}
CSS=${wildcard *.css}

SVG_SOURCES=hierarchie-repertoires.svg joede-power-symbol.svg
SVG_PDF=$(SVG_SOURCES:.svg=.pdf)

LTXMAKE=$(shell ls /usr/include/LaTeX.mk 2>/dev/null)

all: poly-intro-unix.pdf html

poly-intro-unix.pdf: poly-intro-unix.tex $(SVG_PDF)

%.pdf: %.svg
	inkscape --export-pdf=$@ $<

ifneq ($(LTXMAKE),)
LU_MASTER= poly-intro-unix
PDFLATEX_OPTIONS= -shell-escape
include LaTeX.mk
else
ifeq ($(RUBBER),)
COMPILE=pdflatex -shell-escape $< > /dev/null; pdflatex $<
else
COMPILE=rubber $(RFLAGS) -d $<
endif

poly-intro-unix.pdf: poly-intro-unix.tex
	$(COMPILE)


poly-intro-unix.book.ps: poly-intro-unix.pdf
	pdf2ps poly-intro-unix.pdf
	lpbook poly-intro-unix.ps

endif

.PHONY: clean
clean::
	rm -f *.aux *.idx *.ilg *.ind *.log *.out *.toc
	rm -f poly-intro-unix.pdf poly-intro-unix.book.ps poly-intro-unix.ps
	rm -f poly-intro-unix.tex poly-intro-unix.html

EMACS = HOME=$(PWD) emacs --no-site-file --eval '(package-initialize)'

.PHONY: org-info
org-info:
	@$(EMACS) --batch -l org-info.el

poly-intro-unix.tex: poly-intro-unix.org org2tex.el
	$(EMACS) --batch --eval '(message (org-version))'
	$(EMACS) --visit=$< --batch  -l org2tex.el  --kill


poly-intro-unix.html: poly-intro-unix.org org2html.el
	$(EMACS) --batch --eval '(message (org-version))'
	$(EMACS) --visit=$< --batch -l htmlize.el -l org2html.el  --kill

.PHONY: install html upload-chamilo

html: poly-intro-unix.html $(SVG) $(JS) $(CSS)
	-$(RM) -r $@/
	mkdir $@
# npm install html-inline -g to get it. It's OK if you don't have it.
	html-inline $< > $@/index.html || cp $< $@/index.html
	cp $(SVG) $(JS) $(CSS) $@/

install: poly-intro-unix.pdf
	cp $< ~/WWW/IMG/pdf/poly-intro-unix.pdf

poly-intro-unix.zip: poly-intro-unix.pdf html
	cd ../ && zip -r poly-intro-unix/poly-intro-unix.zip \
		poly-intro-unix/poly-intro-unix.pdf poly-intro-unix/html

upload-chamilo: poly-intro-unix.zip
	CHAMILO_USER=$(CHAMILO_USER) ../common/upload-chamilo.sh poly-intro-unix.zip
