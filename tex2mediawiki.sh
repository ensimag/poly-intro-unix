#! /bin/sh

usage () {
            cat << EOF
Usage: $(basename $0) [options]

Translate most LaTeX macros into mediawiki syntax. Proof-read and
correct manually afterwards ...

Options:
	--help	This help message.
	--level Title level (default: 2, i.e. \chapter{foo} -> == foo ==)
EOF
}

level=2

while test $# -ne 0; do
    case "$1" in
        "--help"|"-h")
            usage
            exit 0
            ;;
        "--level")
	    shift
	    level=$1
            ;;
        *)
            echo "unrecognized option $1"
            usage
            exit 1
            ;;
    esac
    shift
done

chapter=""
section="="
subsection="=="
subsubsection="==="
paragraph="===="

for l in $(seq $level); do
    chapter="$chapter"=
    section="$section"=
    subsection="$subsection"=
    subsubsection="$subsubsection"=
    paragraph="$paragraph"=
done

sed -e 's@\\chapter{\(.*\)}@'"$chapter \\1 $chapter@g" \
    -e 's@\\section{\(.*\)}@'"$section \\1 $section@g" \
    -e 's@\\subsection{\(.*\)}@'"$subsection \\1 $subsection@g" \
    -e 's@\\subsubsection{\(.*\)}@'"$subsubsection \\1 $subsubsection@g" \
    -e 's@\\paragraph{\(.*\)}@'"$paragraph \\1 $paragraph@g" \
    -e 's@\\source{\([^}]*\)}@'"<code>\\1</code>@g" \
    -e 's@\\meta{\([^}]*\)}@'"''\\1''@g" \
    -e 's@\\begin{code}@<pre>@g' \
    -e 's@\\end{code}@</pre>@g' \
    -e 's@\\label{.*}@@g' \
    -e 's@\\index{[^}]*}@@g' \
    -e 's@\\home@~@g' \
    -e 's@\\textbackslash *@\\@g' \
    -e 's@\\Tab *@'"''tab''@g" \
    -e 's@\\_@_@g' \
    -e 's@{}@@g' \
